import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:proyectosena/ui/Registrar Hoja de Vida Equipos/Registrar Equipos_Screen.dart';
import 'package:proyectosena/ui/correctivo/correctivo_screen.dart';
import 'package:proyectosena/ui/preventivo/Preventivo_Screen.dart';



  class HomePage extends StatelessWidget {

    @override
    Widget build(BuildContext context) {
      return Scaffold(
          appBar: AppBar(
            title: Text('HOJA DE VIDA MANTENIMIENTOS EQUIPOS'),
            backgroundColor: Colors.blueGrey,
          ),
          drawer: SideDrawer(),
          body: Container(
              color: Colors.tealAccent,

              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [

                    Expanded(
                        flex: 8,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Center(
                                  child: Text(
                                    'BIENIVIDOS',
                                    style: TextStyle(
                                      color: Colors.red,
                                        fontSize: 40.0,
                                        fontWeight: FontWeight.w500,
                                        fontStyle: FontStyle.italic),

                                  )

                              )
             
                            ]
                        )

                    )
                  ]
              )

          )
      );
    }
  }
















class SideDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
      DrawerHeader(
      child: Center(
        child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.asset('assets/images/sena.jpg',
            width: 100,
            height: 100,),
          SizedBox(height: 15,),
          Text("CENTRO DE COMERCIO Y SERVICIOS",
            style: TextStyle(color: Colors.black),)
        ],
      ),
    ),
    decoration: BoxDecoration(
    color: Colors.white,
    ),
    ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text('Inicio'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(Icons.shopping_cart),
            title: Text('Mantenimiento equipos'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(Icons.border_color),
            title: Text(''),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text('Salir'),
            onTap: () => {Navigator.of(context).pop()},
          ),
        ],
      ),
    );
  }
}


